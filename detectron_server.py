import multiprocessing
import os
from functions import run_yolact_server, run_detectron_server

if __name__ == '__main__':

    multiprocessing.set_start_method('spawn')

    ML_MODEL_VAR = os.getenv('ML_MODEL')
    if ML_MODEL_VAR == 'virtual':
        print('Running Detectron server with virtual model')
        print('Serving virtual on ports 8765 and 8766')
        print('Serving virtual on ports 8765 and 8766')
        print('Serving virtual fast on ports 8769 and 8770')
        d1 = multiprocessing.Process(target=run_detectron_server,
                                     args=(8765, '/opt/avena/detect_server/ml_vision_input_coppelia',))
        d2 = multiprocessing.Process(target=run_detectron_server,
                                     args=(8766, '/opt/avena/detect_server/ml_vision_input_coppelia',))
        d3 = multiprocessing.Process(target=run_yolact_server,
                                     args=(8769, '/opt/avena/detect_server/ml_vision_input_coppelia_yolact',))
        d4 = multiprocessing.Process(target=run_yolact_server,
                                     args=(8770, '/opt/avena/detect_server/ml_vision_input_coppelia_yolact',))
        d1.start()
        d2.start()
        d3.start()
        d4.start()
    elif ML_MODEL_VAR == 'real':
        print('Running Detectron server with real model')
        print('Serving real on ports 8767 and 8768')
        print('Serving real on ports 8767 and 8768')
        print('Serving fast real on ports 8771 and 8772')

        d1_blender = multiprocessing.Process(target=run_detectron_server,
                                             args=(8767, '/opt/avena/detect_server/ml_vision_input_blender',))
        d2_blender = multiprocessing.Process(target=run_detectron_server,
                                             args=(8768, '/opt/avena/detect_server/ml_vision_input_blender',))
        # uncomment this when blender model ready
        # d3_blender = multiprocessing.Process(target=run_yolact_server,
        #                                      args=(8771, '/opt/avena/detect_server/ml_vision_input_blender_yolact',))
        # d4_blender = multiprocessing.Process(target=run_yolact_server,
        #                                      args=(8772, '/opt/avena/detect_server/ml_vision_input_blender_yolact',))

        d3_blender = multiprocessing.Process(target=run_yolact_server,
                                             args=(8771, '/opt/avena/detect_server/ml_vision_input_coppelia_yolact',))
        d4_blender = multiprocessing.Process(target=run_yolact_server,
                                             args=(8772, '/opt/avena/detect_server/ml_vision_input_coppelia_yolact',))
        d1_blender.start()
        d2_blender.start()
        d3_blender.start()
        d4_blender.start()
    else:
        print('Running Detectron server with virtual and real models')
        print('Serving virtual on ports 8765 and 8766')
        print('Serving real on ports 8767 and 8768')
        print('Serving fast virtual on ports 8769 and 8770')
        print('Serving fast real on ports 8771 and 8772')

        d1_blender = multiprocessing.Process(target=run_detectron_server,
                                             args=(8767, '/opt/avena/detect_server/ml_vision_input_blender',))
        d2_blender = multiprocessing.Process(target=run_detectron_server,
                                             args=(8768, '/opt/avena/detect_server/ml_vision_input_blender',))
        # uncomment this when blender model ready
        # d3_blender = multiprocessing.Process(target=run_yolact_server,
        #                                      args=(8771, '/opt/avena/detect_server/ml_vision_input_blender_yolact',))
        # d4_blender = multiprocessing.Process(target=run_yolact_server,
        #                                      args=(8772, '/opt/avena/detect_server/ml_vision_input_blender_yolact',))
        d3_blender = multiprocessing.Process(target=run_yolact_server,
                                             args=(8771, '/opt/avena/detect_server/ml_vision_input_coppelia_yolact',))
        d4_blender = multiprocessing.Process(target=run_yolact_server,
                                             args=(8772, '/opt/avena/detect_server/ml_vision_input_coppelia_yolact',))

        d1 = multiprocessing.Process(target=run_detectron_server,
                                     args=(8765, '/opt/avena/detect_server/ml_vision_input_coppelia',))
        d2 = multiprocessing.Process(target=run_detectron_server,
                                     args=(8766, '/opt/avena/detect_server/ml_vision_input_coppelia',))
        d3 = multiprocessing.Process(target=run_yolact_server,
                                     args=(8769, '/opt/avena/detect_server/ml_vision_input_coppelia_yolact',))
        d4 = multiprocessing.Process(target=run_yolact_server,
                                     args=(8770, '/opt/avena/detect_server/ml_vision_input_coppelia_yolact',))

        d1.start()
        d2.start()
        d3.start()
        d4.start()
        d1_blender.start()
        d2_blender.start()
        d3_blender.start()
        d4_blender.start()
