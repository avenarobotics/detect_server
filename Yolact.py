from mmdet.apis import init_detector, inference_detector
import glob
import time
from pycocotools import mask
import numpy as np
import os
import yaml


class Yolact:
    def __init__(self, input_weights_path):
        self.det_treshold = 0.7
        self.input_weights_path = input_weights_path
        # Specify the path to model config and checkpoint file
        self.config_file = os.path.join(input_weights_path, 'custom_config.py')
        self.class_names_file = os.path.join(input_weights_path, 'class_names.yaml')
        # load weights path
        weights_list = glob.glob(os.path.join(self.input_weights_path + "/*.pth"))
        assert len(
            weights_list) == 1, 'ERROR: Exactly one .pth weights file has to be present in detectron2_inference_weights ' \
                                'directory '
        self.checkpoint_file = weights_list[0]

        # build the model from a config file and a checkpoint file
        self.model = init_detector(self.config_file, self.checkpoint_file, device='cuda:0')
        # self.classmap = ['banana', 'blade', 'bowl', 'broccoli', 'broccoli_handle', 'broccoli_head', 'carrot',
        #                  'cucumber',
        #                  'cutting_board', 'handle', 'knife', 'lipton', 'milk', 'onion', 'onion_chives', 'onion_root',
        #                  'orange',
        #                  'plate', 'spatula', 'spatula_tool']
        with open(self.class_names_file, 'r')as f:
            parsed_class_names = yaml.load(f, Loader=yaml.FullLoader)
            num_classes = len(parsed_class_names['MODEL']['NAMES'])
            self.num_classes = num_classes
            self.classmap = parsed_class_names['MODEL']['NAMES']



    def detect_image(self, img):
        start = time.time()
        result = inference_detector(self.model, img)
        result_dict = {
            "classes": list(),
            "masks": list(),
            "scores": list(),
            "class_ids": list()
        }
        classes_list = [cls for cls in result[0]]
        masks_list = [msk for msk in result[1]]
        # for x in masks_list:
        #     for mask in x:
        #         mask = 1*mask
        #         mask *= 255
        #         mask = np.fliplr(x)
        #         mask = np.rot90(x)

        c = 0
        for tpl in zip(classes_list, masks_list):
            m = 0
            for el in tpl[0]:
                if self.det_treshold < el[4]:
                    # appending class names if the detection confidence gt threshold
                    result_dict["classes"].append(self.classmap[c])
                    # appending confidence score values which are at index 4 of the instances np array
                    result_dict["scores"].append(str(el[4]))
                    # appending numeric class ids
                    result_dict["class_ids"].append(c + 1)
                    # get mask of the current instance
                    msk = tpl[1][m]
                    msk = msk.astype(np.uint8)
                    msk *= 255
                    msk = np.fliplr(msk)
                    msk = np.rot90(msk)
                    # encode the mask with rle and append to results dict
                    res = mask.encode(np.asfortranarray(msk))
                    res["counts"] = res["counts"].decode()
                    result_dict["masks"].append(res)
                m += 1
            c += 1
        return result_dict
